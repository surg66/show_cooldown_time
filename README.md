# Desctiption
Show cooldown time - Don't Starve Together modification.

This mod lets you see the cooldown time items in inventory. Additionally, it lets you use the Status Announcement mod to tell your friends whether your watch's ability is ready or not, and if it isn't, how much time it has left.
You can choose language in the options.

[Link to mod in Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2598087493)
