--Spanish
GLOBAL.STRINGS.SHOWCOOLDOWNTIME =
{
    MY_READY = "Mi %s está listo para ser utilizado.",
    MY_TIMELEFT = "Mi %s estará listo en %d minutos y %d segundos.",
    MY_SECONDSLEFT = "Mi %s estará listo en %d segundos.",
    THIS_READY = "Este %s está listo para ser utilizado.",
    THIS_TIMELEFT = "Este %s estará listo en %d minutos %d segundos.",
    THIS_SECONDSLEFT = "Este %s estará listo en %d segundos."
}
